package products;

import java.util.ArrayList;

public class Jeans extends Product {

    public Jeans(ArrayList items, Double price, String originalLocation, String description) {

        super("jeans", 20.89, "Austin Texas", "Some blue jeans with holes. The holes were definitely put there on purpose.");
    }
}
