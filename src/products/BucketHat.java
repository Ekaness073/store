package products;

import java.util.ArrayList;

public class BucketHat extends Product {

    public BucketHat(ArrayList items, Double price, String originalLocation, String description) {

        super("bucket hat", 58.93, "Lynwood Washington REI", "Beige Bucket hat with adjustable slider." );
    }
}
