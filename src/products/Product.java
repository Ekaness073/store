package products;

import com.sun.tools.javac.Main;

import java.util.ArrayList;

public class Product extends Main {

    private Double price;
    private String originalLocation;
    private String description;

    public Product(ArrayList items, Double price, String originalLocation, String description){
        super.items = items;
        this.price = price;
        this.originalLocation = originalLocation;
        this.description = description;
    }

    public String productList(){
        return (super.items + ": $" + this.price + ", " + this.originalLocation + ", " + this.description);
    }
}
