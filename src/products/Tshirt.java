package products;

import java.util.ArrayList;

public class Tshirt extends Product {

    public Tshirt(ArrayList items, Double price, String originalLocation, String description) {

        super("t-shirt" , 18.49, "America", "The official KnockOff Amazon T-Shirt. Comes in Green, White, and Red");
    }
}
