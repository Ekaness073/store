import products.*;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {


    public void main(String[] args){
        Scanner scan = new Scanner(System.in);
        System.out.println("Welcome to KnockOff Amazon. Please enter 'list' for a full list of our products. Enter product name for price, location, and description");

        ArrayList<String> items = new ArrayList<>();
        items.add("t-shirt");
        items.add("jeans");
        items.add("bucket hat");
        items.add("cardigan");
        items.add("knee high socks");

        String input = scan.nextLine();

        if(input.equals("list")){
            System.out.println(items);
        }

        if(input.equals("t-shirt")){
            System.out.println(Tshirt);
        }

        if(input.equals("jeans")) {
            System.out.println(Jeans);
        }

        if(input.equals("bucket hat")) {
            System.out.println(BucketHat);
        }

        if(input.equals("cardigan")) {
            System.out.println(Cardigan);
        }

        if(input.equals("knee high socks")) {
            System.out.println(KneeHighSocks);
        }
    }
 -

}
